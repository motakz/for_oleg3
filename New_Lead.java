import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class New_Lead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub;
		 WebDriver driver = new FirefoxDriver();
		 
		 //navigate to the login page
		driver.get("https://core.futuresimple.com/users/login");
		
		//submit credentials
		driver.findElement(By.id("user_email")).sendKeys("oleg.svergun@gmail.com");
		
	    driver.findElement(By.id("user_password")).sendKeys("killer");
		
		driver.findElement(By.xpath("/html/body/div/form/fieldset/div[3]/div/button")).click();
		
		//Navigate to the Leads page
		driver.findElement(By.id("nav-leads")).click();
		
		//wait until page loaded
		WebDriverWait wait = new WebDriverWait(driver, 40);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("leads-new")));
		
		//add a new lead
		driver.findElement(By.id("leads-new")).click();	
		driver.findElement(By.id("lead-first-name")).sendKeys("Oleg");
		driver.findElement(By.id("lead-last-name")).sendKeys("Svergun");
		driver.findElement(By.id("lead-company-name")).sendKeys("Sample Company");
		driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div/div/div[2]/button")).click();
		
		//add a new note for the created lead:
        WebDriverWait wait1 = new WebDriverWait(driver, 40);
		WebElement element1 = wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[3]/div/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/div/form/fieldset/div/textarea")));
        
		//find the note field and submitting a new Note
		driver.findElement(By.xpath("/html/body/div[3]/div/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/div/form/fieldset/div/textarea")).sendKeys("My test note");
	    driver.findElement(By.xpath("/html/body/div[3]/div/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/div/form/fieldset/div/div/button")).click();
		
	   //Wait For Page To Load
	    driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

	    //Check that added note exists
	    driver.findElement(By.xpath("//*[contains(text(),'My test note')]"));
	}

}
